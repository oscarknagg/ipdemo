This is the source code for the KeyLines IP traffic demo (see Wandera Wiki https://snappli.atlassian.net/wiki/display/RAD/IP+traffic+visualisation).

If you are using NodeJS as your server, just run this file 'node server.js'

Running server.js has a dependency on the express module being available, which can be installed (once node is installed) by running

 >  npm install express

If you can't run Node or express just use a simple python server
    
 > python -m SimpleHTTPServer 8080

Then you can navigate to http://localhost:8080 in your web browser to see the demo running

This demo relies on a Neo4j database backend. Download and install neo4j community and run it with the command './bin/neo4j start'. Use the database called ipdemo.db.

The data for this demo can be fetched with the following query to proxy logs.

select
    source,
    destination,
    sum(dataMB) as data,
    count(*) as nrequests
from
(
select
    split_part(address, '.', 1) || '.' || split_part(address, '.', 2) || '.' || split_part(address, '.', 3) || '.XX' as source,
    destinationaddress as destination,
    (datatransferred*1.0) / 1000000 as dataMB,
    startutcinms + startutcinmsfine as unixtime,
    (TIMESTAMP 'epoch' + (startutcinms/1000) * INTERVAL '1 Second ') as datetime,
    activepolicy
from
    proxy_logs
where
    customerid = '259daedb-c506-4644-b0dc-90c51cb5e4f8'
    and startutcinms >= datediff (ms, '1970-01-01', '2016-08-01')
	and startutcinms < datediff (ms, '1970-01-01', '2016-08-02')
	and site != 'cellular-counter'
	and site is not null
)
group by
    source,
    destination

The resultant data is loaded into Neo4j with the following query.

// Load all nodes and relationships from one table
LOAD CSV WITH HEADERS FROM "file:///source_file.csv" AS line
MERGE (s:Source {address: line.source})
MERGE (d:Destination {address: line.destination})
CREATE (s)-[:CONTACTS { data: toFloat(line.data), requests: toInt(line.nrequests)}]->(d);

// Add requests sent and data sent properties
match (s:Source)-[r]->()
with s, sum(r.nrequests) as nsent, sum(r.data) as data
set s.nsent = nsent,
	s.datasent = data

// Add requests received and data received properties
match ()-[r]->(d:Destination)
with d, sum(r.nrequests) as nreceived, sum(r.data) as data
set d.nreceived = nreceived,
	d.datareceived = data
